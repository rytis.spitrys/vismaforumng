import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared.service';
import { Search } from 'src/app/models/search.model';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {
  openNav: boolean =  false;
  changedValue: Search;

  constructor(private shSrv: SharedService) { }

  ngOnInit() {
  }

  showHideNavigation() {
    this.openNav = !this.openNav;
  }

  filterPosts() {
    this.shSrv.getFilterInput(this.changedValue);
  }
}
