import { Component, OnInit } from "@angular/core";
import { fade } from '../shared/animations';

@Component({
  selector: "app-content",
  templateUrl: "./content.component.html",
  styleUrls: ["./content.component.scss"],
  animations: [
    fade
  ]
})
export class ContentComponent implements OnInit {
  perPageLimiter: any = '2';
  constructor() {
  }

  ngOnInit() {

  }

  onChangePageLimit(e: any) {
    this.perPageLimiter = e.target.value;
  }

}
