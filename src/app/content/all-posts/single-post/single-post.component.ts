import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { Router, ActivatedRoute } from '@angular/router';
import { fade } from 'src/app/shared/animations';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss'],
  animations: [
    fade
  ]
})
export class SinglePostComponent implements OnInit {
  @Input() post: Post;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }


  editPost(){
    this.router.navigate([`edit`, this.post.id]);
  }
}
