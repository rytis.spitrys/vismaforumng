import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { fade } from 'src/app/shared/animations';
import { Post } from 'src/app/models/post.model';
import { Status } from 'src/app/shared/form/form.component';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.scss'],
  animations: [
    fade
  ]
})
export class PostEditComponent implements OnInit {
  formStatus = Status.Edit;
  singleLoadedPost: Post;
  id: number;

  constructor(
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activatedRoute.data
    .subscribe((data: { post: Post }) => {
      this.singleLoadedPost = data.post;
    });
  }
}
