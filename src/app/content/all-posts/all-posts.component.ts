import { Component, OnInit, Input,  } from "@angular/core";
import { Post } from "src/app/models/post.model";
import { HttpService } from "src/app/services/http.service";
import { SharedService } from "src/app/services/shared.service";
import { BehaviorSubject, Observable, combineLatest } from "rxjs";
import {
  debounceTime,
  startWith,
  distinctUntilChanged,
  switchMap
} from "rxjs/operators";

@Component({
  selector: "app-all-posts",
  templateUrl: "./all-posts.component.html",
  styleUrls: ["./all-posts.component.scss"]
})
export class AllPostsComponent implements OnInit {
  @Input() set limiter(value: number) {
    this.limitSubject.next(value);
  }

  allPosts$: Observable<Post[]>;
  pageSubject = new BehaviorSubject(1);
  totalCount: number;

  // 2 default reiksme
  readonly limitSubject = new BehaviorSubject(2);
  readonly search$ = this.shSrv.inputChanged.pipe(
    startWith(""),
    debounceTime(500),
    distinctUntilChanged((oldArr, newArr) => {
      return oldArr[0] === newArr[0];
    })
  );

  constructor(private http: HttpService, private shSrv: SharedService) {}

  ngOnInit() {
    this.http.getCount().subscribe(
      response => this.totalCount = response.length
    );
    this.allPosts$ = this.filterPosts();
    this.limitSubject.subscribe(() => this.pageSubject.next(1));
  }

  filterPosts() {
    return combineLatest(
      this.search$,
      this.limitSubject,
      this.pageSubject
    ).pipe(
      switchMap(([search, limit, pageNumber]) => {
        return this.http.filterPosts(search, limit * pageNumber);
      })
    );
  }

  onMorePostsClick() {
    const snapshot = this.pageSubject.getValue();
    this.pageSubject.next(snapshot + 1);
  }

  showMoreButton(): boolean {
    const limit = +this.limitSubject.getValue();
    const page = +this.pageSubject.getValue();
    return this.totalCount > limit * page;
  }

}
