import { Component, OnInit } from '@angular/core';
import { Status } from 'src/app/shared/form/form.component';


@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss'],
})

export class NewPostComponent implements OnInit {
  formStatus = Status.New;

  constructor() {}

  ngOnInit() {

  }
}
