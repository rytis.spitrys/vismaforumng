import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component'
import { NewPostComponent } from './content/new-post/new-post.component';
import { PostEditComponent } from './content/all-posts/single-post/post-edit/post-edit.component';
import { PostEditResolverService } from './services/post-edit-resolver.service';
import { Page404Component } from './shared/page404/page404.component';


const routes: Routes = [
  {path: '', component: ContentComponent, pathMatch: 'full'},
  {path: 'new', component: NewPostComponent},
  {path: 'edit/:id', component: PostEditComponent, resolve: {
    post: PostEditResolverService
  }},
  {path: 'notfound', component: Page404Component},
  {path: '**', redirectTo: 'notfound'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
