import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotifierModule } from 'angular-notifier';

import { AppComponent } from './app.component';
import { NavbarComponent } from './header/navbar/navbar.component';
import { ContentComponent } from './content/content.component';
import { AllPostsComponent } from './content/all-posts/all-posts.component';
import { SinglePostComponent } from './content/all-posts/single-post/single-post.component';
import { NewPostComponent } from './content/new-post/new-post.component';
import { PostEditComponent } from './content/all-posts/single-post/post-edit/post-edit.component';
import { FormComponent } from './shared/form/form.component';
import { Page404Component } from './shared/page404/page404.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { JsonServerInterceptor } from './services/json-server.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ContentComponent,
    AllPostsComponent,
    SinglePostComponent,
    NewPostComponent,
    PostEditComponent,
    FormComponent,
    Page404Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NotifierModule.withConfig( {
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JsonServerInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
