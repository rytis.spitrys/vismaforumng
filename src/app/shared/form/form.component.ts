import { Component, OnInit, Input } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Post } from "src/app/models/post.model";
import { HttpService } from "src/app/services/http.service";
import { fade } from "src/app/shared/animations";

export enum Status {
  New = 'New',
  Edit = 'Edit'
}

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"],
  animations: [fade]
})
export class FormComponent implements OnInit {
  postForm: FormGroup;
  @Input() post: Post;
  @Input() status: Status;
  statuses = Status;


  constructor(
    private router: Router,
    private http: HttpService,
  ) {}

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.postForm = new FormGroup({
      title: new FormControl("", {
        validators: [Validators.required, Validators.minLength(1)]
      }),
      content: new FormControl("", {
        validators: [Validators.required, Validators.minLength(1)]
      }),
      author: new FormControl("", {
        validators: [Validators.required, Validators.minLength(1)]
      }),
      date: new FormControl("", {
        validators: [Validators.required, Validators.minLength(1)]
      })
    });

    this.status === Status.Edit ? this.patchInputValues() : '';
  }

  patchInputValues() {
    return this.postForm.patchValue(this.post);
  }

  onSubmit() {
    if(!this.postForm.valid) {
      return;
    }
    const formValue = this.postForm.value;
    const newPost: Post = {
      ...formValue
    };
    if (this.status === Status.New) {
      this.http.newPost(newPost).subscribe(() => {
        this.onCancel();
      });
    } else {
      this.http.updatePost(this.post.id, newPost).subscribe(() => {
        this.onCancel();
      });
    }
  }

  isInvalid(fieldName: string) {
    return (
      !this.postForm.get(fieldName).valid &&
      this.postForm.get(fieldName).touched
    );
  }

  onDelete() {
    this.http.deletePost(this.post.id).subscribe(() => {
      this.onCancel();
    });
  }

  onCancel() {
    this.router.navigate(["/"]);
  }
}
