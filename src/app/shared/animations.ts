import { trigger, transition, style, animate } from "@angular/animations";

export const fade = trigger("fade", [
  transition(":enter", [
    style({ backgroundColor: "white", opacity: 0 }),
    animate(200)
  ])
]);
