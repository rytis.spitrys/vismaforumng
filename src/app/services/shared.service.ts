import { Injectable } from '@angular/core';
import { Search } from '../models/search.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  inputChanged = new Subject<Search>();
  filterInput: Search;


  constructor() { }

  getFilterInput(input: Search) {
    this.filterInput = input;
    this.inputChanged.next(this.filterInput);
  }
}
