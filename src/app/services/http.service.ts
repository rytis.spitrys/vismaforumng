import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post.model';
import { Search } from '../models/search.model';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient) {}
  url = `http://localhost:3000/posts/`;

  loadMore(query: Search, limit: number) {
    return this.http.get<Post[]>(`${this.url}`, {
      params: {
        title_like: query || '',
        _limit: String(limit),
      }
    });
  }

  filterPosts(query: Search, limit: number) {
    return this.http.get<Post[]>(`${this.url}`, {
      params: {
        title_like: query || '',
        _limit: String(limit),
        _sort:'id',
        _order:'desc'
      }
    });
  }

  getCount() {
    return this.http.get<Post[]>(`${this.url}`).pipe(
      take(1),
      map(value => value)
    )
  }

  newPost(post: Post) {
    return this.http.post<Post>(`${this.url}`, post);
  }

  getSinglePost(id: number) {
    return this.http.get<Post>(`${this.url}${id}`);
  }

  deletePost(id: number) {
    return this.http.delete(`${this.url}${id}`);
  }

  updatePost(id: number, post: Post) {
    return this.http.put<Post>(`${this.url}${id}`, post);
  }

}
