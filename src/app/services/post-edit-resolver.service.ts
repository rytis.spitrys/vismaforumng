import { Injectable }             from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
}                                 from '@angular/router';
import { Observable, of, EMPTY }  from 'rxjs';
import { mergeMap, catchError }         from 'rxjs/operators';
import { HttpService } from './http.service';
import { Post } from '../models/post.model';
 
@Injectable({
  providedIn: 'root',
})
export class PostEditResolverService implements Resolve<Post> {
  constructor(private http: HttpService, private router: Router) {}
 
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Post> | Observable<never> {
    let id = Number(route.paramMap.get('id'));
 
    return this.http.getSinglePost(id).pipe(
      mergeMap(post => {
        if (post) {
          return of(post);
        }
      }),
      catchError(error => {
        this.router.navigate(['notfound']);
        return EMPTY;
      })
    );
  }
}