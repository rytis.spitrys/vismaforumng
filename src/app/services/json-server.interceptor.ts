import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { Post } from "../models/post.model";
import { NgxSpinnerService } from "ngx-spinner";
import { Observable, throwError } from "rxjs";
import { finalize, tap, catchError } from "rxjs/operators";
import { NotifierService } from 'angular-notifier';

export class JsonServerInterceptor implements HttpInterceptor {
  constructor(private spinner: NgxSpinnerService, private notifierService: NotifierService) {}

  intercept(req: HttpRequest<Post>, next: HttpHandler): Observable<HttpEvent<any>> {
    // i copy req because its immutable
    const newRequest = req.clone({
      headers: req.headers.set
      ('Authorization', 'token-here')
    });
    console.log(newRequest);

    this.spinner.show();
    return next.handle(newRequest).pipe(
        tap((event: HttpResponse<any>) => {
          if(event.status === 200 || event.status === 201) {
            if(newRequest.method === "POST" ) {
              this.notifierService.notify( 'success', 'Added new post!' );
            }else if(newRequest.method === "DELETE") {
              this.notifierService.notify( 'warning', 'Post deleted!' );
            }else if(newRequest.method === "PUT") {
              this.notifierService.notify( 'info', 'Post edited!' );
            }
          }
        }),
        catchError((error: HttpErrorResponse) => {
          if (error.status === 404) {
            this.notifierService.notify('error', 'Something went wrong! Try again!');
          }
          return throwError(error);
        }),
        finalize(() => {
          this.spinner.hide();
        })
    ) 
  }
}
