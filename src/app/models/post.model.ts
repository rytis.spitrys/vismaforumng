export interface Post {
    title: string,
    content: string,
    author: string,
    date: string,
    id: number
}
